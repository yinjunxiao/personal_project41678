import tkinter

import math

class JiSuanQi:

    def __init__(self):
        self.root = tkinter.Tk()

        self.root.title('计算器')

        self.root.minsize(230, 250)

        self.shownum = tkinter.StringVar()
        self.shownum.set('0')

        self.presslist = []

        self.ispresssign = False

        self.issumeql = False

        self.issqrt = False

        self.setwindow()


        self.root.mainloop()

    def setwindow(self):

        label = tkinter.Label(self.root, textvariable = self.shownum, bg='white', font=('黑体', 15), anchor='e', bd=10)
        label.place(x=5, y=20, width=220, height=50)

        btn1 = tkinter.Button(self.root, text='7', command=lambda: self.pressnum('7'))
        btn1.place(x=5, y=100, width=40, height=25)

        btn2 = tkinter.Button(self.root, text='8', command=lambda: self.pressnum('8'))
        btn2.place(x=50, y=100, width=40, height=25)

        btn3 = tkinter.Button(self.root, text='9', command=lambda: self.pressnum('9'))
        btn3.place(x=95, y=100, width=40, height=25)

        btndiv = tkinter.Button(self.root, text='/', command=lambda: self.presssign('/'))
        btndiv.place(x=140, y=100, width=40, height=25)

        btnper = tkinter.Button(self.root, text='c', command=self.clear)
        btnper.place(x=185, y=100, width=40, height=25)

        btn4 = tkinter.Button(self.root, text='4', command=lambda: self.pressnum('4'))
        btn4.place(x=5, y=130, width=40, height=25)

        btn5 = tkinter.Button(self.root, text='5', command=lambda: self.pressnum('5'))
        btn5.place(x=50, y=130, width=40, height=25)

        btn6 = tkinter.Button(self.root, text='6', command=lambda: self.pressnum('6'))
        btn6.place(x=95, y=130, width=40, height=25)

        btntake = tkinter.Button(self.root, text='*', command=lambda: self.presssign('*'))
        btntake.place(x=140, y=130, width=40, height=25)

        btndif = tkinter.Button(self.root, text='+/-', command=lambda: self.pressnum('-'))
        btndif.place(x=185, y=130, width=40, height=25)

        btn7 = tkinter.Button(self.root, text='1', command=lambda: self.pressnum('1'))
        btn7.place(x=5, y=160, width=40, height=25)

        btn8 = tkinter.Button(self.root, text='2', command=lambda: self.pressnum('2'))
        btn8.place(x=50, y=160, width=40, height=25)

        btn9 = tkinter.Button(self.root, text='3', command=lambda: self.pressnum('3'))
        btn9.place(x=95, y=160, width=40, height=25)

        btnsub = tkinter.Button(self.root, text='-', command=lambda: self.presssign('-'))
        btnsub.place(x=140, y=160, width=40, height=25)

        btnetc = tkinter.Button(self.root, text='=', command=self.sumeql)
        btnetc.place(x=185, y=160, width=40, height=55)

        btn0 = tkinter.Button(self.root, text='0', command=lambda: self.pressnum('0'))
        btn0.place(x=5, y=190, width=85, height=25)

        btnback = tkinter.Button(self.root,text = '←',command = self.backspaces)
        btnback.place(x = 5,y = 220 ,width=85, height=25)

        btnadd = tkinter.Button(self.root, text='+', command=lambda: self.presssign('+'))
        btnadd.place(x=140, y=190, width=40, height=25)

        btnp = tkinter.Button(self.root, text='.', command=lambda: self.pressnum('.'))
        btnp.place(x=95, y=190, width=40, height=25)

        btnps = tkinter.Button(self.root, text='√', command= self.sqrs)
        btnps.place(x=95, y=220, width=40, height=25)

    #开根号
    def sqrs(self):

        if '-' in self.shownum.get():
            self.shownum.set('负数不能开平方')

            self.issqrt = True

        else:

            result = math.sqrt(float(self.shownum.get()))
            print(result)

            self.shownum.set(result)

            self.issqrt = True

            # 显示
    def pressnum(self,num):

        #global ispresssign

        #global issumeql
        #判断运算结果是否为真
        if self.issumeql == True:
            self.shownum.set('0')

            self.issumeql = False
        #判断是否加入了运算符号
        elif self.ispresssign == True:
            self.shownum.set('0')

            self.ispresssign = False

        elif  self.issqrt == True:
            self.shownum.set('0')
            self.issqrt = False

       # elif self.presslist == []:
        #    self.shownum.set('0')

        self.oldnum = self.shownum.get()

        if self.oldnum == '0' and num != '.':
            # # 加入小数点
            # if num == '.':
            #     #if oldnum.startswith('0'):
            #     self.shownum.set(oldnum + num)
            # if num in oldnum:
            #
            #     pass
            # else:

            self.shownum.set(num)
        # 加入小数点
        elif num == '.' :
                #如果已经输入了小数点，那么再输入小数点则无效
            if num in self.oldnum:
                pass
            else:
                self.shownum.set(self.oldnum+num)

        else:#判断正负号
            if num == '-':
                if self.oldnum.startswith('-'):
                    self.shownum.set(self.oldnum[1:])
                else:
                    self.shownum.set('-' + self.oldnum)
            else:
                self.shownum.set(self.oldnum + num)

            # 退格
    def backspaces(self):
        if  self.shownum.get() == '0' and self.shownum.get() == '':
            self.shownum.set('0')
            return

        else:
            num = len(self.shownum.get())

            if num > 1:
                self.oldnum = self.shownum.get()
               # print(self.oldnum)
                self.oldnum = self.oldnum[0:num - 1]
                self.shownum.set(self.oldnum)

            else:
                self.shownum.set('0')


                            # 运算过程

    def presssign(self,sign):
        #global presslist
        #global ispresssign

        self.oldnum = self.shownum.get()

        self.presslist.append(self.oldnum)

        # if sign in self.presslist:
        #
        #     self.presslist[1] = sign
        #
        # else:
        self.presslist.append(sign)

        self.ispresssign = True

    # 运算结果
    def sumeql(self):
        #global presslist

        #global issumeql

        self.oldnum = self.shownum.get()

        self.presslist.append(self.oldnum)

        print(self.presslist)

        # unswer = ''.join(self.presslist)
        #
        # if unswer[-1] in '+-*/':
        #     unswer = unswer[0:-2]

        try:

            result = eval(''.join(self.presslist))

            self.shownum.set(result)

            self.presslist.clear()

            self.issumeql = True

        except:

            self.shownum.set('除数不能为0')

            self.presslist.clear()

            self.issumeql = True

    # 清空操作
    def clear(self):
        self.shownum.set('0')






